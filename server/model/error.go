package model

import "errors"

//自定义错误
var (
    ErrorUserNotExists = errors.New("用户不存在")
    ErrorUserExists    = errors.New("用户已存在")
    ErrorUserPwd       = errors.New("密码错误")
)