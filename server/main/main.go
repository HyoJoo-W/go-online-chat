package main

import (
    "fmt"
    "gitee.com/HyoJoo-W/ChatRoom/server/model"
    "net"
    "time"
)

//处理和客户端的通信,保持连接
func processConnection(conn net.Conn) {

    defer conn.Close()
    //调用总控
    processor := &Processor{
        Conn: conn,
    }
    err := processor.processMessage()
    if err != nil {
        fmt.Println("客户端与服务器端通信协程错误, err= ", err)
    }

}

//初始化UserDao
func initUserDao(){
    //pool是redis.go中的全局变量
    model.MyUserDao = model.NewUserDao(pool)
}

func main() {

    //服务器启动时,初始化连接池
    initPool("localhost:6379", 16, 0, 300*time.Second)
    initUserDao()

    //服务器端
    fmt.Println("服务器在8889端口监听...")
    listen, err := net.Listen("tcp", ":8889")
    // defer listen.Close()

    if err != nil {
        fmt.Println("net.listen err=", err)
        return
    }
    //监听成功，等待客户端连接
    for {
        fmt.Println("等待客户端连接...")
        conn, err := listen.Accept()
        if err != nil {
            fmt.Println("listen.Accept err=", err)
        }

        //连接成功，启动一个协程与客户端保持通讯
        go processConnection(conn)
    }
}
