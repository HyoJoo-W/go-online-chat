package main

import (
    "github.com/gomodule/redigo/redis"
    "time"
)

var pool *redis.Pool

//程序启动时,初始化连接池
func initPool(address string, maxIdle, maxActive int, idleTimeout time.Duration) {
    pool = &redis.Pool{
        MaxIdle:     maxIdle,     //最大空闲连接数
        MaxActive:   maxActive,   //表示与数据库的最大连接数，0表示无限制
        IdleTimeout: idleTimeout, //最大空闲时间
        Dial: func() (redis.Conn, error) { //初始化连接IP
            return redis.Dial("tcp", address)
        },
    }
}
