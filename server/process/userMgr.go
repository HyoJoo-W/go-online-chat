package process

import "fmt"

var (
    userMgr *UserMgr
)

// UserMgr 在线列表在服务器端只有一个
type UserMgr struct {
    onlineUsers map[int]*UserProcess
}

//初始化 userMgr
func init() {
    userMgr = &UserMgr{
        onlineUsers: make(map[int]*UserProcess, 1024),
    }
}

// AddOnlineUser 添加在线User
func (this *UserMgr) AddOnlineUser(up *UserProcess) {
    this.onlineUsers[up.UserId] = up
}

// DeleteOnlineUser 删除在线User
func (this *UserMgr) DeleteOnlineUser(userId int) {
    delete(this.onlineUsers, userId)
}

// GetAllOnlineUsers 查询所有在线User
func (this *UserMgr) GetAllOnlineUsers() map[int]*UserProcess {
    return this.onlineUsers
}

// GetOnlineUserById 查询指定id的UserProcess
func (this *UserMgr) GetOnlineUserById(userId int) (up *UserProcess, err error) {
    up, ok := this.onlineUsers[userId]
    if !ok {
        //查询的指定User不在线
        err = fmt.Errorf("用户 %d 不在线", userId)
        return
    }
    return
}
