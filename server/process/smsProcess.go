package process

import (
    "encoding/json"
    "fmt"
    "gitee.com/HyoJoo-W/ChatRoom/common/message"
    "gitee.com/HyoJoo-W/ChatRoom/server/utils"
    "net"
)

type SmsProcess struct {
}

// SendGroupMes 服务器端转发消息
func (this *SmsProcess) SendGroupMes(mes *message.Message) {
    //取出群聊消息
    var smsMes message.SmsMes
    err := json.Unmarshal([]byte(mes.Data), &smsMes)
    if err != nil {
        fmt.Println("[群聊]服务器端反序列化消息失败, err = ", err)
        return
    }

    //序列化mes
    data, err := json.Marshal(mes)
    if err != nil {
        fmt.Println("[群聊]序列化消息失败, err = ", err)
        return
    }

    //遍历服务器端在线用户列表,将群聊消息转发给其他人
    for id, up := range userMgr.onlineUsers {
        if id == smsMes.UserId {
            continue
        }
        this.SendGroupMesToEach(data, up.Conn)
    }
}

// SendGroupMesToEach 转发给每个人
func (this *SmsProcess) SendGroupMesToEach(data []byte, conn net.Conn) {
    tf := &utils.Transfer{
        Conn: conn,
    }
    err := tf.WritePkg(data)
    if err != nil {
        fmt.Println("[群聊]服务器端发送消息失败, err = ", err)
        return
    }
}
