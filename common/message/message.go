package message

import (
    "gitee.com/HyoJoo-W/ChatRoom/common/entity"
)

const (
    LoginMesType       = "LoginMes"       //客户端发送的登录请求消息类型
    LoginResMesType    = "LoginResMes"    //服务器端发送的登录响应消息类型
    RegisterMesType    = "RegisterMes"    //客户端发送的注册请求消息类型
    RegisterResMesType = "RegisterResMes" //服务器端发送的注册响应消息类型

    NotifyUserStatusMesType = "NotifyUserStatusMes" //服务器端推送的消息类型

    SmsMesType = "SmsMes" //客户端发送的聊天消息类型
)

//用户在线状态常量
const (
    UserOnline = iota
    UserOffline
    UserBusy
)

// Message 统一封装消息
type Message struct {
    Type string `json:"type"` //消息类型
    Data string `json:"data"` //消息内容
}

// LoginMes 客户端发送登录消息
type LoginMes struct {
    UserId   int    `json:"userId"`   //用户id
    UserPwd  string `json:"userPwd"`  //用户密码
    UserName string `json:"userName"` //用户名
}

// LoginResMes 服务器端响应消息
type LoginResMes struct {
    Code    int    `json:"code"`  //状态码，500表示服务器端错误，200表示登录成功
    Error   string `json:"error"` //错误信息
    UserIds []int  //保存用户切片
}

// RegisterMes 客户端发送注册消息
type RegisterMes struct {
    User entity.User `json:"user"`
}

// RegisterResMes 服务器端响应消息
type RegisterResMes struct {
    Code  int    `json:"code"`  //状态码,400表示该用户已经存在,200表示注册成功
    Error string `json:"error"` //错误信息
}

// NotifyUserStatusMes 服务器端推送消息类型
type NotifyUserStatusMes struct {
    UserId int `json:"userId"` //用户ID
    Status int `json:"status"` //用户状态
}

// SmsMes 发送消息类型
type SmsMes struct {
    Content     string `json:"content"` //消息内容
    entity.User                         //匿名结构体
}

// SmsResMes 服务器端响应消息类型
type SmsResMes struct {
}
