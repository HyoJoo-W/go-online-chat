package entity

// User 定义用户结构体
type User struct {
    //保证序列化成功,添加json
    UserId     int    `json:"usesId"`
    UserPwd    string `json:"userPwd"`
    UserName   string `json:"userName"`
    UserStatus int    `json:"userStatus"` //用户在线状态
}
