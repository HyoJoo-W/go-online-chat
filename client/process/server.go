package process

import (
    "encoding/json"
    "fmt"
    "gitee.com/HyoJoo-W/ChatRoom/client/utils"
    "gitee.com/HyoJoo-W/ChatRoom/common/message"
    "net"
    "os"
)

// ShowMenu 显示登录后的界面
func ShowMenu() {
    fmt.Println("恭喜登录成功")
    fmt.Println("1 显示在线用户列表")
    fmt.Println("2 发送消息")
    fmt.Println("3 信息列表")
    fmt.Println("4 退出系统")
    fmt.Println("请选择(1-4):")

    var key int
    var content string
    smsProcess := &SmsProcess{}

    fmt.Scanf("%d\n", &key)
    switch key {
    case 1:
        //fmt.Println("显示在线用户列表")
        outputOnlineUsers()
    case 2:
        //fmt.Println("发送消息")
        fmt.Println("请输入想群发的消息:")
        fmt.Scanf("%s\n", &content)
        smsProcess.SendGroupMes(content)
    case 3:
        fmt.Println("信息列表")
    case 4:
        fmt.Println("你选择了退出系统")
        os.Exit(0)
    default:
        fmt.Println("输入选项错误")
    }
}

//和服务器端保持通信
func serverProcessMes(conn net.Conn) {
    //创建transfer实例,一直读取服务器端的消息
    tf := &utils.Transfer{
        Conn: conn,
    }
    for {
        fmt.Println("客户端正在等待读取服务器端数据")
        mes, err := tf.ReadPkg()
        if err != nil {
            fmt.Println("tf.ReadPkg err = ", err)
            return
        }
        //读取到服务器端推送的消息
        switch mes.Type {
        case message.NotifyUserStatusMesType:
            //取出服务器端推送的消息
            var notifyUserStatusMes message.NotifyUserStatusMes
            err := json.Unmarshal([]byte(mes.Data), &notifyUserStatusMes)
            if err != nil {
                return
            }
            updateUserStatus(&notifyUserStatusMes)
        case message.SmsMesType:
            //取出服务器端转发的群聊消息
            outputGroupMes(&mes)
        default:
            fmt.Println("[推送]服务器端推送了未知类型的消息")
        }
        //fmt.Println("读取到的消息为：", mes)
    }
}
