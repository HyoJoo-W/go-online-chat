package process

import (
    "encoding/json"
    "fmt"
    "gitee.com/HyoJoo-W/ChatRoom/common/message"
)

//处理服务器端转发的群聊消息

//显示转发的群聊消息
func outputGroupMes(mes *message.Message) {
    //1 反序列化mes.Data
    var smsMes message.SmsMes
    err := json.Unmarshal([]byte(mes.Data), &smsMes)
    if err != nil {
        fmt.Println("[群聊]反序列化失败, err = ", err.Error())
        return
    }

    info := fmt.Sprintf("用户id: %d\t 说话:%s", smsMes.UserId, smsMes.Content)
    fmt.Println(info)
    fmt.Println()

}
