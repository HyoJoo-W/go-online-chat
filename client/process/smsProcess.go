package process

import (
    "encoding/json"
    "fmt"
    "gitee.com/HyoJoo-W/ChatRoom/client/utils"
    "gitee.com/HyoJoo-W/ChatRoom/common/message"
)

//群发消息处理器

type SmsProcess struct {
}

// SendGroupMes 发送群聊消息
func (this *SmsProcess) SendGroupMes(content string) (err error) {
    //1 创建Message
    var mes message.Message
    mes.Type = message.SmsMesType

    //2 创建SmsMes实例,组装消息
    var smsMes message.SmsMes
    smsMes.Content = content
    smsMes.UserId = CurUser.UserId
    smsMes.UserStatus = CurUser.UserStatus

    //3 序列化smsMes,组装到mes中
    data, err := json.Marshal(smsMes)
    if err != nil {
        fmt.Println("[群发]序列化消息体失败, err = ", err.Error())
        return
    }
    mes.Data = string(data)

    //4 序列化mes,发送给服务器
    data, err = json.Marshal(mes)
    if err != nil {
        fmt.Println("[群发]序列化消息失败, err = ", err.Error())
        return
    }

    //5 发送给服务器
    tf := &utils.Transfer{
        Conn: CurUser.Conn,
    }
    err = tf.WritePkg(data)
    if err != nil {
        fmt.Println("[群发]发送消息失败, err = ", err.Error())
        return
    }
    return
}
