package process

import (
    "fmt"
    "gitee.com/HyoJoo-W/ChatRoom/client/model"
    "gitee.com/HyoJoo-W/ChatRoom/common/entity"
    "gitee.com/HyoJoo-W/ChatRoom/common/message"
)

//客户端维护的在线用户表
var onlineUsers map[int]*entity.User = make(map[int]*entity.User, 10)

// CurUser 客户端记录当前User(用户登录成功后,完成对CurUser的初始化)
var CurUser model.CurUser

//处理服务器端推送的NotifyUserStatusMes
func updateUserStatus(notifyUserStatusMes *message.NotifyUserStatusMes) {
    //判断是否已经存在
    user, ok := onlineUsers[notifyUserStatusMes.UserId]
    if !ok {
        user = &entity.User{
            UserId: notifyUserStatusMes.UserId,
        }
    }
    user.UserStatus = notifyUserStatusMes.Status
    onlineUsers[notifyUserStatusMes.UserId] = user

    outputOnlineUsers()
}

//显示在线用户
func outputOnlineUsers() {
    fmt.Println("当前在线用户列表为:")
    for id, _ := range onlineUsers {
        fmt.Println("用户id:\t",id)
    }
}
