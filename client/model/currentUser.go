package model

import (
    "gitee.com/HyoJoo-W/ChatRoom/common/entity"
    "net"
)



// CurUser 客户端记录当前User
type CurUser struct {
    Conn net.Conn
    entity.User
}
